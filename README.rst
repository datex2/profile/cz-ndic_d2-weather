==========================
Format Specification Suite
==========================
:uri: cz-ndic_d2-weather-v1.1
:format: DATEX II Situation Publication - Weather

This repository provides tools and files for given format:

- schema
- sample(s)
- documentation
- test suite
- unified `tox` based interface for related tools

About described format
======================

For all details, see `FORMAT.yaml`.

Using provided tools
====================

For all details, see `tox.rst`.

Changelog
=========

version 1.0.0 

- Innitial format and documentation

version 1.0.1

- revision of the documentation, proofreading, changes to examples, some texts regarding locations and concepts have been modified to be more specific.

version 1.1.0

- added feedtype to schema and samples
